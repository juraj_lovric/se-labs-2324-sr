from query_handler_base import QueryHandlerBase
import random
import requests
import json

class WeatherHandler(QueryHandlerBase):
    def can_process_query(self, query):
        if "weather" in query:
            return True
        return False

    def process(self, query):
        input = query.split()
        city=input[1]

        try:
            result = self.call_api(city)
            txt = result["current"]["temp_c"]
            self.ui.say(txt)
        except Exception as e: 
            self.ui.say("Oh no! There was an error.")
            self.ui.say("Try something else!")



    def call_api(self, city):
        url = "https://weatherapi-com.p.rapidapi.com/current.json"

        querystring = {"q":city}

        headers = {
                "X-RapidAPI-Key": "f443f42f6dmsh4af34c9b93021e1p1a6f93jsn5e7f8051cdd7",
	            "X-RapidAPI-Host": "weatherapi-com.p.rapidapi.com"

        }

        response = requests.get(url, headers=headers, params=querystring)

        return json.loads(response.text)



