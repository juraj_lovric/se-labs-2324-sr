from query_handler_base import QueryHandlerBase
import random
import requests
import json

class ChuckNorrisHandler(QueryHandlerBase):
    def can_process_query(self, query):
        if "chuck" in query:
            return True
        return False
 
    def process(self, query):
 
 
        try:
            result = self.call_api()
            joke = result["value"]
            self.ui.say(f"{joke}")
 
        except: 
 
            self.ui.say("Try something else!")


    def call_api(self):
        url = "https://matchilling-chuck-norris-jokes-v1.p.rapidapi.com/jokes/random"

        headers = {
                "X-RapidAPI-Key": "f443f42f6dmsh4af34c9b93021e1p1a6f93jsn5e7f8051cdd7",
	            "X-RapidAPI-Host": "matchilling-chuck-norris-jokes-v1.p.rapidapi.com"
        }

        response =  requests.get(url, headers=headers)

        return json.loads(response.text)