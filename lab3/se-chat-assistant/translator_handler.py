from query_handler_base import QueryHandlerBase
import random
import requests
import json

class TraslatorHandler(QueryHandlerBase):
    def can_process_query(self, query):
        if "translate" in query:
            return True
        return False

    def process(self, query):
        input = query.split()
        source_language = input[1]
        target_language = input[2]
        text= " ".join(input[3:])

        try:
            result = self.call_api(source_language, target_language,text)
            txt = result["data"]["translatedText"]
            self.ui.say(txt)
        except Exception as e: 
            self.ui.say("Oh no! There was an error.")
            self.ui.say("Try something else!")



    def call_api(self, source_language,target_language,text):
        url = "https://text-translator2.p.rapidapi.com/translate"

        payload = {"source_language": source_language ,"target_language": target_language,"text": text}

        headers = {
                "content-type": "application/x-www-form-urlencoded",
	            "X-RapidAPI-Key": "f443f42f6dmsh4af34c9b93021e1p1a6f93jsn5e7f8051cdd7",
	            "X-RapidAPI-Host": "text-translator2.p.rapidapi.com"
        }

        response = requests.post(url, data=payload, headers=headers)

        return json.loads(response.text)



